package main

import (
	"context"
	"encoding/json"
	"fmt"
	"hello/fizzbuzz"
	"io"
	"log"
	"net/http"
	"os"
	"os/signal"
	"strconv"
	"strings"
	"syscall"
	"time"

	_ "github.com/go-sql-driver/mysql"
	"github.com/gorilla/mux"
	"github.com/spf13/viper"
)

func main() {
	viper.SetConfigName("config")
	viper.AddConfigPath(".") // ระบุ path ของ config file
	viper.AutomaticEnv()     // อ่าน value จาก ENV variable
	// แปลง _ underscore ใน env เป็น . dot notation ใน viper
	viper.SetEnvKeyReplacer(strings.NewReplacer(".", "_"))
	// อ่าน config
	err := viper.ReadInConfig()
	if err != nil {
		panic(fmt.Errorf("fatal error config file: %s \n", err))
	}
	host := viper.GetString("app.host")
	port := viper.GetString("app.port")
	writeTimeOut := viper.GetInt("app.writetimout")
	readTimeOut := viper.GetInt("app.readtimeout")

	//Open when install MariaDB Docker Successfuly
	// dbHost := viper.GetString("app.db.host")
	// dbPort := viper.GetString("app.db.port")
	// dbName := viper.GetString("app.db.database")
	// dbUser := viper.GetString("app.db.user")
	// dbPass := viper.GetString("app.db.pass")
	// dbCon := dbUser + ":" + dbPass + "@tcp(" + dbHost + ":" + dbPort + ")/" + dbName
	//db, err := sql.Open("mysql", dbCon)

	// if err != nil {
	// 	log.Print(err.Error())
	// } else {
	// 	log.Print("connect DB : " + dbHost + " successful")
	// }
	//defer db.Close()

	r := mux.NewRouter()
	r.HandleFunc("/{number}", helloHandle)

	srv := &http.Server{
		Handler: r,
		Addr:    host + ":" + port,
		// Good practice: enforce timeouts for servers you create!
		WriteTimeout: time.Duration(writeTimeOut) * time.Second,
		ReadTimeout:  time.Duration(readTimeOut) * time.Second,
	}
	// println("started....")

	// log.Fatal(srv.ListenAndServe())

	// Start server
	errChan := make(chan error)
	go func(errChan chan error) {
		if err := srv.ListenAndServe(); err != nil && err != http.ErrServerClosed {
			errChan <- err
		}
	}(errChan)
	// Gracefully Shutdown
	// Make channel listen for signals from OS
	gracefulStop := make(chan os.Signal, 1)
	signal.Notify(gracefulStop, syscall.SIGTERM)
	signal.Notify(gracefulStop, syscall.SIGINT)
	signal.Notify(gracefulStop, os.Interrupt)

	select {
	case err := <-errChan:
		log.Fatalln(err)
	case <-gracefulStop:
		log.Println("Shutting down server after requests finish...")
		ctx, cancel := context.WithTimeout(context.Background(), time.Second*30)
		defer cancel()

		if err := srv.Shutdown(ctx); err != nil {
			fmt.Printf("Error shutting down server %s", err)
			log.Fatalln(err)
		} else {
			fmt.Println("Server gracefully stopped")
		}

		//Open when install MariaDB Docker Successfuly
		// if err := db.Close(); err != nil {
		// 	fmt.Printf("Error closing db connection %s", err)
		// 	log.Fatalln(err)
		// } else {
		// 	fmt.Println("DB connection gracefully closed")
		// }

		log.Println("Server exiting now")
		break
	}

}

func helloHandle(w http.ResponseWriter, r *http.Request) {

	time.Sleep(10 * time.Second)

	vars := mux.Vars(r)

	n, err := strconv.Atoi(vars["number"])

	if err != nil {
		w.WriteHeader(http.StatusBadGateway)
		io.WriteString(w, err.Error())
		return
	}
	w.WriteHeader(http.StatusOK)
	w.Header().Add("Content-Type", "application/json")
	result := fizzbuzz.Say(n)
	mapD := map[string]string{"message": result}
	mapB, _ := json.Marshal(mapD)
	w.Write(mapB)
	return
}
